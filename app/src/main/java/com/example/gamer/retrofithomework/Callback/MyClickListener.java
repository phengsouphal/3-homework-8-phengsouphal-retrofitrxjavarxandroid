package com.example.gamer.retrofithomework.Callback;

import android.view.View;

import com.example.gamer.retrofithomework.entity.Category;

public interface MyClickListener {

    void onClicked(int position, View view);

}
