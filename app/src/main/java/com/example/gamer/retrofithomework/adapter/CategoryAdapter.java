package com.example.gamer.retrofithomework.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.gamer.retrofithomework.Callback.MyClickListener;
import com.example.gamer.retrofithomework.MainActivity;
import com.example.gamer.retrofithomework.R;
import com.example.gamer.retrofithomework.entity.CateResponse;
import com.example.gamer.retrofithomework.entity.Category;

import java.util.ArrayList;
import java.util.List;



public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>
{
        Context context;
        List<Category> categoryList;
        MyClickListener clickListener;
        public CategoryAdapter(Context t){
            context = t;
            categoryList=new ArrayList<>();
        }



    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
      clickListener= (MyClickListener) recyclerView.getContext();

    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_row,viewGroup,false);
        return new CategoryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {

            Category category=categoryList.get(i);
            categoryViewHolder.txtcatename.setText(category.getCateName());
            categoryViewHolder.txtcatedes.setText(category.getDes());
            RequestOptions options = new RequestOptions();
            options.placeholder(R.drawable.knong_dai);

            Glide.with(context).load(category.getIconName()).apply(options).into(categoryViewHolder.imageCate);


    }



    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    public Category getCategory(int position){
            return this.categoryList.get(position);
    }

    public void addMoreCate(List<Category> categories){
            categoryList.addAll(categories);
            notifyItemInserted(0);
    }

    public void updateItemOf(Category category){
            for (Category temp: this.categoryList){
                if (temp.getId()==category.getId()){
                    temp.setCateName(category.getCateName());
                    temp.setDes(category.getDes());
                    notifyItemChanged(this.categoryList.indexOf(temp));

                }
            }
    }

    public void setData(List<Category> categoryList)
    {
            this.categoryList = categoryList;
            notifyDataSetChanged();
    }



    class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtcatename,txtsub, txtcatedes;
        ImageView imgMore,imageCate;



        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            txtcatedes=itemView.findViewById(R.id.txtCateDes);
            txtcatename=itemView.findViewById(R.id.txtCatename);
            txtsub=itemView.findViewById(R.id.txtsub);
            imageCate=itemView.findViewById(R.id.imageCate);
            imgMore=itemView.findViewById(R.id.btnimgmore);
            imgMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClicked(getAdapterPosition(),v);


        }
    }
}
