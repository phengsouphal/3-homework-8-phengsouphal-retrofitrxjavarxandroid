package com.example.gamer.retrofithomework.entity;


import com.google.gson.annotations.SerializedName;

public abstract class UpdateCateRespone {

    @SerializedName("msg")
    private String msg;

    @SerializedName("status")
    private boolean status;

    @SerializedName("code")
    private String code;

    private Category category;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "UpdateCateRespone{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", code='" + code + '\'' +
                ", category=" + category +
                '}';
    }
}
