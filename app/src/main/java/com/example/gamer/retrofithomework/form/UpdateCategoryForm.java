package com.example.gamer.retrofithomework.form;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateCategoryForm {


    @SerializedName("cate_name")
    public String cate_name;

    @SerializedName("des")
    public String des;

    @SerializedName("keywords")
    public List<String> keywords;

    @SerializedName("id")
    public int id;

    @SerializedName("icon_name")
    public String icon_name;


    @SerializedName("status")
    public boolean status;


    public UpdateCategoryForm(String cate_name, String des, List<String> keywords, int id, String icon_name, boolean status) {
        this.cate_name = cate_name;
        this.des = des;
        this.keywords = keywords;
        this.id = id;
        this.icon_name = icon_name;
        this.status = status;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon_name() {
        return icon_name;
    }

    public void setIcon_name(String icon_name) {
        this.icon_name = icon_name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
