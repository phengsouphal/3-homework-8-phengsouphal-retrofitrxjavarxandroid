package com.example.gamer.retrofithomework;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gamer.retrofithomework.entity.Category;

import org.greenrobot.eventbus.EventBus;


public class UpdateCateFragment extends DialogFragment {

    private static final String CATE="cate";
    private Category category;

    EditText edCatename,edCatedes;
    Button btnupdate;


    public static UpdateCateFragment newInstance(Category category) {
      UpdateCateFragment fragment=new UpdateCateFragment();
        Bundle bundle=new Bundle();
        bundle.putParcelable(CATE ,category);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_update_cate, container, false);

        btnupdate=view.findViewById(R.id.btnCateupdate);
        edCatedes=view.findViewById(R.id.edupdatecatedes);
        edCatename=view.findViewById(R.id.edupdatecatename);


        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setCateName(edCatename.getText().toString());
                category.setDes(edCatedes.getText().toString());
                Toast.makeText(getContext(),"Update",Toast.LENGTH_SHORT).show();

                EventBus.getDefault().post(new CateUpdateEvent(category));
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments()!=null){
            category=getArguments().getParcelable(CATE);
            edCatename.setText(category.getCateName());
            edCatedes.setText(category.getDes());
        }
    }



}
