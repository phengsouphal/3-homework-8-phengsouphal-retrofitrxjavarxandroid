package com.example.gamer.retrofithomework.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostCategory {

    @Expose
    @SerializedName("status")
    private boolean status;
    @Expose
    @SerializedName("keywords")
    private List<String> keywords;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("icon_name")
    private String icon_name;
    @Expose
    @SerializedName("des")
    private String des;
    @Expose
    @SerializedName("cate_name")
    private String cate_name;



    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon_name() {
        return icon_name;
    }

    public void setIcon_name(String icon_name) {
        this.icon_name = icon_name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCate_name() {
        return cate_name;
    }

    public void setCate_name(String cate_name) {
        this.cate_name = cate_name;
    }
}
